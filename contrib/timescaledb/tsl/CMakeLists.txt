add_subdirectory(test)
add_subdirectory(src)
set(PROJECT_TRUNK_DIR ${CMAKE_SOURCE_DIR}/../..)
set(PROJECT_OPENGS_DIR ${PROJECT_TRUNK_DIR} CACHE INTERNAL "")
set(PROJECT_SRC_DIR ${PROJECT_OPENGS_DIR}/src CACHE INTERNAL "")
set(Third_party_library $ENV{BINARYLIBS}/kernel/dependency/libobs/comm/include)
add_definitions(-DPGXC)
add_definitions(-DUSE_SPQ)
include_directories(${PROJECT_SRC_DIR}/include ${PROJECT_INCLUDE_DIR} ${Third_party_library})

install(CODE "
  file(REMOVE \"$ENV{GAUSSHOME}/share/postgresql/extension/timescaledb--1.7.4.sql\")
  file(COPY \"../og-timescaledb1.7.4.sql\" DESTINATION \"$ENV{GAUSSHOME}/share/postgresql/extension\")
  file(RENAME \"$ENV{GAUSSHOME}/share/postgresql/extension/og-timescaledb1.7.4.sql\" \"$ENV{GAUSSHOME}/share/postgresql/extension/timescaledb--1.7.4.sql\")
")