//	Greenplum Database
//	Copyright (C) 2018 Pivotal, Inc.

#ifndef SPQOPT_CParseHandlerCostModelTest_H
#define SPQOPT_CParseHandlerCostModelTest_H

#include "spqos/base.h"

namespace spqdxl
{
class CParseHandlerCostModelTest
{
public:
	static spqos::SPQOS_RESULT EresUnittest();
};
}  // namespace spqdxl
#endif	// SPQOPT_CParseHandlerCostModelTest_H
