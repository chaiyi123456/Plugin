//	Greenplum Database
//	Copyright (C) 2018 Pivotal, Inc.

#ifndef SPQOPT_CParseHandlerOptimizerConfigSerializeTest_H
#define SPQOPT_CParseHandlerOptimizerConfigSerializeTest_H

#include "spqos/base.h"

namespace spqdxl
{
class CParseHandlerOptimizerConfigSerializeTest
{
public:
	static spqos::SPQOS_RESULT EresUnittest();
};

}  // namespace spqdxl
#endif	// SPQOPT_CParseHandlerOptimizerConfigSerializeTest_H
