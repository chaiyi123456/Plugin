//	Greenplum Database
//	Copyright (C) 2017 Pivotal Software, Inc

#ifndef SPQOS_CHashSetIterTest_H
#define SPQOS_CHashSetIterTest_H

#include "spqos/base.h"

namespace spqos
{
// Static unit tests
class CHashSetIterTest
{
public:
	// unittests
	static SPQOS_RESULT EresUnittest();
	static SPQOS_RESULT EresUnittest_Basic();

};	// class CHashSetIterTest
}  // namespace spqos

#endif	// !SPQOS_CHashSetIterTest_H

// EOF
