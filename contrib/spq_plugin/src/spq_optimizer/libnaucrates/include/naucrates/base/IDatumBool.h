//---------------------------------------------------------------------------
//	Greenplum Database
//	Copyright (C) 2011 EMC Corp.
//
//	@filename:
//		IDatumBool.h
//
//	@doc:
//		Base abstract class for bool representation
//---------------------------------------------------------------------------
#ifndef SPQNAUCRATES_IDatumBool_H
#define SPQNAUCRATES_IDatumBool_H

#include "spqos/base.h"

#include "naucrates/base/IDatum.h"

namespace spqnaucrates
{
//---------------------------------------------------------------------------
//	@class:
//		IDatumBool
//
//	@doc:
//		Base abstract class for bool representation
//
//---------------------------------------------------------------------------
class IDatumBool : public IDatum
{
private:
	// private copy ctor
	IDatumBool(const IDatumBool &);

public:
	// ctor
	IDatumBool(){};

	// dtor
	virtual ~IDatumBool(){};

	// accessor for datum type
	virtual IMDType::ETypeInfo
	GetDatumType()
	{
		return IMDType::EtiBool;
	}

	// accessor of boolean value
	virtual BOOL GetValue() const = 0;

	// can datum be mapped to a double
	BOOL
	IsDatumMappableToDouble() const
	{
		return true;
	}

	// map to double for stats computation
	CDouble
	GetDoubleMapping() const
	{
		if (GetValue())
		{
			return CDouble(1.0);
		}

		return CDouble(0.0);
	}

	// can datum be mapped to LINT
	BOOL
	IsDatumMappableToLINT() const
	{
		return true;
	}

	// map to LINT for statistics computation
	LINT
	GetLINTMapping() const
	{
		if (GetValue())
		{
			return LINT(1);
		}
		return LINT(0);
	}

	// byte array representation of datum
	virtual const BYTE *
	GetByteArrayValue() const
	{
		SPQOS_ASSERT(!"Invalid invocation of MakeCopyOfValue");
		return NULL;
	}

	// does the datum need to be padded before statistical derivation
	virtual BOOL
	NeedsPadding() const
	{
		return false;
	}

	// return the padded datum
	virtual IDatum *
	MakePaddedDatum(CMemoryPool *,	// mp,
					ULONG			// col_len
	) const
	{
		SPQOS_ASSERT(!"Invalid invocation of MakePaddedDatum");
		return NULL;
	}

	// does datum support like predicate
	virtual BOOL
	SupportsLikePredicate() const
	{
		return false;
	}

	// return the default scale factor of like predicate
	virtual CDouble
	GetLikePredicateScaleFactor() const
	{
		SPQOS_ASSERT(!"Invalid invocation of DLikeSelectivity");
		return false;
	}
};	// class IDatumBool

}  // namespace spqnaucrates


#endif	// !SPQNAUCRATES_IDatumBool_H

// EOF
