create schema test_json_pretty;
set current_schema to 'test_json_pretty';
-- test for basic functionality of json_replace
select JSON_PRETTY('{"a": 43}');
 json_pretty 
-------------
   {        +
   "a": 43  +
 }
(1 row)

select JSON_PRETTY('true');
 json_pretty 
-------------
   true
(1 row)

select JSON_PRETTY('false');
 json_pretty 
-------------
   false
(1 row)

select JSON_PRETTY('1');
 json_pretty 
-------------
   1
(1 row)

select JSON_PRETTY('"hello world"');
   json_pretty   
-----------------
   "hello world"
(1 row)

select JSON_PRETTY('[1,3,4]');
 json_pretty 
-------------
   [        +
   1,       +
   3,       +
   4        +
 ]
(1 row)

select JSON_PRETTY('["a"]');
 json_pretty 
-------------
   [        +
   "a"      +
 ]
(1 row)

select JSON_PRETTY('["a", "b"]');
 json_pretty 
-------------
   [        +
   "a",     +
   "b"      +
 ]
(1 row)

select JSON_PRETTY('["a", 232]');
 json_pretty 
-------------
   [        +
   "a",     +
   232      +
 ]
(1 row)

select JSON_PRETTY('["a", true, false]');
 json_pretty 
-------------
   [        +
   "a",     +
   true,    +
   false    +
 ]
(1 row)

select JSON_PRETTY('["a", ["a"]]');
 json_pretty 
-------------
   [        +
   "a",     +
   [        +
     "a"    +
   ]        +
 ]
(1 row)

select JSON_PRETTY('["a", {"a": "hello"}]');
   json_pretty    
------------------
   [             +
   "a",          +
   {             +
     "a": "hello"+
   }             +
 ]
(1 row)

select JSON_PRETTY('[111, {"a": "hello"}]');
   json_pretty    
------------------
   [             +
   111,          +
   {             +
     "a": "hello"+
   }             +
 ]
(1 row)

-- test for empty object and array
select JSON_PRETTY('{}');
 json_pretty 
-------------
   {}
(1 row)

select JSON_PRETTY('[]');
 json_pretty 
-------------
   []
(1 row)

select JSON_PRETTY('{"a": {}}');
 json_pretty 
-------------
   {        +
   "a": {}  +
 }
(1 row)

select JSON_PRETTY('{"a": []}');
 json_pretty 
-------------
   {        +
   "a": []  +
 }
(1 row)

select JSON_PRETTY('[{}]');
 json_pretty 
-------------
   [        +
   {}       +
 ]
(1 row)

select JSON_PRETTY('[[]]');
 json_pretty 
-------------
   [        +
   []       +
 ]
(1 row)

select JSON_PRETTY('["a", []]');
 json_pretty 
-------------
   [        +
   "a",     +
   []       +
 ]
(1 row)

select JSON_PRETTY('["a", {}]');
 json_pretty 
-------------
   [        +
   "a",     +
   {}       +
 ]
(1 row)

-- test for null
select JSON_PRETTY(null);
 json_pretty 
-------------
 
(1 row)

-- test for invalid json doc
select JSON_PRETTY('{aa,b}');
ERROR:  Invalid JSON text in argument 1 to function json_pretty.
CONTEXT:  referenced column: json_pretty
select JSON_PRETTY('[*]');
ERROR:  Invalid JSON text in argument 1 to function json_pretty.
CONTEXT:  referenced column: json_pretty
select JSON_PRETTY('["a, []]');
ERROR:  Invalid JSON text in argument 1 to function json_pretty.
CONTEXT:  referenced column: json_pretty
select JSON_PRETTY('{'a':3}');
ERROR:  syntax error at or near "a"
LINE 1: select JSON_PRETTY('{'a':3}');
                              ^
SELECT JSON_PRETTY('');
ERROR:  Invalid JSON text in argument 1 to function json_pretty.
CONTEXT:  referenced column: json_pretty
SELECT JSON_PRETTY(122323);
ERROR:  Invalid data type for JSON data in argument 1 to function json_pretty
CONTEXT:  referenced column: json_pretty
select JSON_PRETTY('{"a": }');
ERROR:  Invalid JSON text in argument 1 to function json_pretty.
CONTEXT:  referenced column: json_pretty
select JSON_PRETTY('{a: 43}');
ERROR:  Invalid JSON text in argument 1 to function json_pretty.
CONTEXT:  referenced column: json_pretty
-- test for large json doc
SELECT JSON_PRETTY('{  
    "Person": {    
       "Name": "Homer", 
       "Age": 39,
       "Hobbies": ["Eating", "Sleeping"]  
    }
 }');
     json_pretty      
----------------------
   {                 +
   "Person": {       +
     "Age": 39,      +
     "Name": "Homer",+
     "Hobbies": [    +
       "Eating",     +
       "Sleeping"    +
     ]               +
   }                 +
 }
(1 row)

select JSON_PRETTY('{"a":[{"age": 43, "name": "lihua"}, [[[[43,33, []]]]], "hello"]}');
      json_pretty      
-----------------------
   {                  +
   "a": [             +
     {                +
       "age": 43,     +
       "name": "lihua"+
     },               +
     [                +
       [              +
         [            +
           [          +
             43,      +
             33,      +
             []       +
           ]          +
         ]            +
       ]              +
     ],               +
     "hello"          +
   ]                  +
 }
(1 row)

-- test for call other functions
SELECT JSON_PRETTY(json_build_object('name', 'Lihua'));
    json_pretty    
-------------------
   {              +
   "name": "Lihua"+
 }
(1 row)

select JSON_PRETTY(json_build_object('name', 'Lihua', 'age', '43'));
    json_pretty    
-------------------
   {              +
   "age": "43",   +
   "name": "Lihua"+
 }
(1 row)

select json_build_object('ob',JSON_PRETTY('{"a": 43}'));
 json_build_object 
-------------------
 {"ob" :   {      +
   "a": 43        +
 }}
(1 row)

-- test for table
create temp table test (
    textjson json
);
insert into test values
('{"a": {"b": 32, "c":"hello"}, "d": 0.3443}'),
('["dog", "pig", {"a": "here"}]'),
('["pig", "dog", {"a": "there"}]'),
('{"a": "abc","b": {"b": "abc", "a": "abc"}}');
select JSON_PRETTY(textjson) from test;
   json_pretty    
------------------
   {             +
   "a": {        +
     "b": 32,    +
     "c": "hello"+
   },            +
   "d": 0.3443   +
 }
   [             +
   "dog",        +
   "pig",        +
   {             +
     "a": "here" +
   }             +
 ]
   [             +
   "pig",        +
   "dog",        +
   {             +
     "a": "there"+
   }             +
 ]
   {             +
   "a": "abc",   +
   "b": {        +
     "a": "abc", +
     "b": "abc"  +
   }             +
 }
(4 rows)

insert into test values
(JSON_PRETTY('{"a": 43, "b": {"c": true}}'));
select * from test;
                  textjson                  
--------------------------------------------
 {"a": {"b": 32, "c":"hello"}, "d": 0.3443}
 ["dog", "pig", {"a": "here"}]
 ["pig", "dog", {"a": "there"}]
 {"a": "abc","b": {"b": "abc", "a": "abc"}}
   {                                       +
   "a": 43,                                +
   "b": {                                  +
     "c": true                             +
   }                                       +
 }
(5 rows)

drop schema test_json_pretty cascade;
reset current_schema;
